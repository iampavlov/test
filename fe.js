var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 40, 
'Виктория Заровская', 40, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 0, 
'Александр Малов', 0,
'Николай Фролов', 10,
'Олег Боровой', 0
];


var students = studentsAndPoints.filter(function(item){
  return typeof item == 'string';
});

var points = studentsAndPoints.filter(function(item){
  return typeof item == 'number';
});


console.info('Список студентов:');
students.forEach(function(item, i){
  console.log('Студент ' + item + ' набрал ' + points[i] + ' баллов');
})

var maxPoint = Math.max.apply(null, points);;
var index = points.indexOf(maxPoint);
console.log('!!! ' + students[index] + ' набрал больше всех - ' + maxPoint + ' !!!');


console.info('--- Список студентов после увеличения баллов:');
var points = points.map(function(item, i){
  if (students[i] == 'Ирина Овчинникова' || students[i] == 'Александр Малов') {
    return item += 30;
  } else return item
});
students.forEach(function(item, i){
  console.log('Студент %s набрал %d баллов', item, points[i]);
})

//GET TOP

var top = [];
var bin = points;
for (var i = 0; i < bin.length; i++){
	var max = Math.max.apply(null, bin);
	var index = bin.indexOf(max);
	top.push(students[index], max);
	bin[index] = -1;
}

function getTop(num){
  for (var i=0; i<num*2; i+=2) {
    console.log(top[i]+' — '+top[i+1]+' баллов');
  };
}

console.info('----TOP 3----');
getTop(3);

console.info('----TOP 5----');
getTop(5);