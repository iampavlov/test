var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 30, 
'Виктория Заровская', 30, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 60, 
'Александр Малов', 0
];

function show () {
  console.log('Студент %s набрал %d баллов', this.name, this.point)
}

// переводим массив в массив объектов
var students = new Array();
for (var i = 0; i < studentsAndPoints.length; i+=2) {
  students.push({
    name: studentsAndPoints[i],
    point: studentsAndPoints[i+1],
    show: show,
  })
};

console.info('----вывод одного студента:');
console.log(students[4].show());


// добавить новых студенторв
students.push({
  name: 'Николай Фролов',
  point: 0,
  show: show,
})
students.push({
  name: 'Олег Боровой',
  point: 0,
  show: show,
})

//увеличить баллы
students.forEach(function(item){
  if (item.name == 'Ирина Овчинникова' || item.name == 'Александр Малов'){
    return item.point += 30;
  } else if (item.name == 'Николай Фролов') {
    return item.point += 10;
  }
})


console.info('Вывести всех, у кого больше 30 баллов:');
students.forEach(function(item){
  if(item.point >= 30){
    item.show();
  }
})


// добавить количество сделанных работы
students.forEach(function(item){
  item.worksAmount = item.point/10;
})
//console.log(students);


//Найти по имени
console.info('### Поиск объекта по имени')


students.findByName = function(name) {
  for (var i = students.length - 1; i >= 0; i--) {
    if (name && name == students[i].name) {
      return students[i]
    }
  }
};


console.log('Вернул объект - ' + students.findByName('Николай Фролов'))
console.log('Объект правильный - ' + students.findByName('Николай Фролов').name)

