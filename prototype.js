var studentsAndPoints = [
	'Алексей Петров', 0, 
	'Ирина Овчинникова', 60, 
	'Глеб Стукалов', 30, 
	'Антон Павлович', 30, 
	'Виктория Заровская', 30, 
	'Алексей Левенец', 70, 
	'Тимур Вамуш', 30, 
	'Евгений Прочан', 60, 
	'Александр Малов', 0,
];

//Объявляем методы

function show () {
	console.log('Студент %s набрал %d баллов', this.name, this.point)
}

function add (name, point) {
	this.list.push(name, point);
}

//Конструктор студентов

function Student(name, point) {
	this.name = name;
	this.point = point;
	this.show = show
}

var student1 = new Student ('Ушат Помоев', 100);
var student2 = new Student ('Забег Дебилов', 20);

console.info('Вывод студентов (Student) методом show')
student1.show();
student2.show();

//конструктор групп
function StudentList(groupName, list){
	if (list) {
		this.groupName = groupName;
		this.list = list;
		this.add = add;
	} else {
		this.groupName = groupName;
		this.list = [];
		this.add = add;;
	}
}

var hj2 = new StudentList ('HJ-2', studentsAndPoints);
var html7 = new StudentList ('HTML-7');


//Добавление новых студентов
hj2.add('Камаз Отходов', 100);
hj2.add('Подрыв Устоев', 20);

html7.add('Парад Уродов', 50);
html7.add('Поджог Сараев', 30);

StudentList.prototype.show = function() {
	console.log('Группа ' + this.groupName + ' (' + Math.round(this.list.length / 2) + ' студентов):');
	for (var i = 0; i <= this.list.length -1 ; i+=2) {
		console.log('Студент %s набрал %d баллов', this.list[i], this.list[i+1]);
	}
};

// Переводилка студента
var cuttedStudent = hj2.list.splice(-2, 2);
html7.list.push(cuttedStudent[0], cuttedStudent[1]);
var cuttedStudent = hj2.list.splice(-2, 2);

// Вычисление максимального балла
StudentList.prototype.valueOf = function(){
	return this.list;
}
StudentList.prototype.max = function (){
	var max = Math.max.apply(null, this.list);
	console.log('Максимальный балл — %d', max);
}

// Вывод групп

console.info('------------------');

hj2.show();
hj2.max();
console.info('------------------');

html7.show();
html7.max();
