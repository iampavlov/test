var studentsAndPoints = [
'Алексей Петров', 0, 
'Ирина Овчинникова', 60, 
'Глеб Стукалов', 30, 
'Антон Павлович', 40, 
'Виктория Заровская', 40, 
'Алексей Левенец', 70, 
'Тимур Вамуш', 30, 
'Евгений Прочан', 0, 
'Александр Малов', 0,
'Николай Фролов', 10,
'Олег Боровой', 0
];


var students = studentsAndPoints.filter(function(item){
  return typeof item == 'string';
});

var points = studentsAndPoints.filter(function(item){
  return typeof item == 'number';
});


console.info('Список студентов:');
students.forEach(function(item, i){
  console.log('Студент ' + item + ' набрал ' + points[i] + ' баллов');
})

console.info('Студент набравший максимальный балл:');
points.forEach(function(item, i){
   if (item == Math.max.apply(null, points)){
    console.log(students[i] + ' (' + item + ' баллов)');
   }
});

console.info('Список студентов после увеличения баллов:');
students.map(function(item, i){
  if (item == 'Ирина Овчинникова' || item == 'Александр Малов') {
    points[i] += 30;
  }
});
students.forEach(function(item, i){
  console.log('Студент ' + item + ' набрал ' + points[i] + ' баллов');
})

//GET TOP
console.info('TOP:');
console.log('исходный массив - ' + points);

var clone = [];
var top = points.map(function(item, i){
  var max = Math.max.apply(null, points);
  if (item == max) {
    console.log(item);
    clone.unshift(item);
  } else {
    clone.push(item);
  }
})
console.log(clone);

//console.log(utop);
//console.log(getTop());
/*var quicksort = function(array){
  if(array.length <=1) return array;
  var swapPos = Math.floor((array.length-1)/2);
  var swapValue = array[swapPos], less = [], more = [];
  //console.log('swapPos = '+swapPos);
  //console.log('swapValue = '+swapValue);
  //array = array.slice(0, swapPos).concat(array.slice(swapPos + 1));
  array = array.splice(0, swapPos, array.slice(swapPos + 1));
  //console.log('array - '+array);
  for (var i = 0; i < array.length; i++){
    if (array[i] < swapValue) {
      less.push(array[i]);
      //console.log('less = '+less);
    } else {
      more.push(array[i]);
      //console.log('more = '+more);
    }
  }
  //console.info('-------------------end');
  return (quicksort(more)).concat([swapValue], quicksort(less));
};*/

